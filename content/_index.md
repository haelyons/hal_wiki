---
title: Introduction
type: docs
bookToc: false
---

Hi, I'm **Hélios Aël Lyons**. I occasionally write about programming, human-computer interaction, and sound.

Currently most writings are contained within the [Blog]({{< ref "/posts" >}} "Blog") section. For the motivations behind this website and licensing see the [Meta]({{< ref "/meta" >}} "Meta") page; for the website implementation please see the [Building this website]({{< ref "/posts/building-website" >}} "Building this website") blog post. 

You can read my background and personal links on the [About]({{< ref "/about" >}} "About") page or directly access my [curriculum vitae](https://drive.google.com/file/d/1MoNg0ngrmyq3DLOFTy-mv3Y6lDN-1aaI/view?usp=sharing). The Tools section is currently being built -- have a look at my [GitHub](https://github.com/haelyons) for current projects.

Send your best memes and book recommendations -> helioslyons@icloud.com

![Profile](profile.jpg)  
*A very serious picture.*
Sé Catedral da Guarda (2021)
