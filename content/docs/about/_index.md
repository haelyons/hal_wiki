---
weight: 1
bookFlatSection: true
title: "About"
bookToc: false
---

# About

Hello! I’m Hélios, a French-Australian living in London. I did my BSc at the University of Leeds in [Music, Multimedia, and Electronics](https://courses.leeds.ac.uk/a617/music-multimedia-and-electronics-bsc) and currently work as an Embedded Software Engineer at [disguise](https://www.disguise.one/en/).

I have a variety of interests at the intersection of hardware and software, often applied to signal processing and embedded software though I am passionate in understanding a range of engineering systems, and developing creative, efficient solutions. At the moment my focus is embedded Linux, especially device driver integration and user-space development (mostly C, C++, and Python) for extended reality infrastructure.

Currently in my free time I'm thinking about recommender systems (especially creating personalised models of individuals in games such as Chess), and composing music for theatre (as part of the Missing Theatre Collective). If you have knowledge in these areas and want to chat please reach out :)

## Links

I am available principally via [Discord](https://discordapp.com/users/226869629924933633), [email](mailto:helioslyons@icloud.com), and [LinkedIn](https://www.linkedin.com/in/h%C3%A9lios-lyons-a274a7168/). 

You can view some of my work (mostly from university and small side projects) on [GitHub](https://github.com/haelyons) or view my full [curriculum vitae](https://drive.google.com/file/d/1lTst6v2xOyZYVCU8i_hojRdJ1wf-e-Et/view?usp=sharing). 

I have experimented with video compression artefect art which you can see on [Instagram](https://www.instagram.com/fungal.cell/?hl=en-gb), and semi-regularly compose experimental electronic music which you can listen to on [SoundCloud](https://soundcloud.com/0x0c/tracks) expanding on my background as a harpist.
  
  
<!---
## Freelance

I pursue freelance and contract work in sound and multimedia projects when I have the time, below are some of the projects I have contributed to since coming to the UK:

- [Missing Theatre Collective](https://missingtheatre.cargo.site/) (2023+) - composition, interactive set design for immersive theatre
- [Mechamorphosis](https://vimeo.com/756873254?embedded=true&source=video_title&owner=140931408) by [Alberto Scotti](https://www.linkedin.com/in/alberto-scotti-901747182/) (2022) - foley recording, sound design
- [Almost Human](https://video.eko.com/v/Aq4qap?autoplay=true) by Alberto Scotti (2021) - live recording, dubbing, composition, sound design
- [Greed (ft. Tom Dean)](https://www.youtube.com/watch?v=ZzMYuDWCeZY&ab_channel=AlbertoScotti) by Helios Lyons (2021) - recording, production, composition, and artistic direction
- [Crawling Back for More by High Windows](https://l.facebook.com/l.php?u=https%3A%2F%2Fsoundcloud.com%2Fuser-114330107%2Fcrawling-back-for-more-final%3Ffbclid%3DIwAR3vLE4eXE3SFXebGCvtXibaClIs6ZelWHFEpIz2NN2yXTLah5e1_Lsb9fE&h=AT1ReYj6cyeZiaAUwuHHLJo-zjFPTPQP_Sk73-i6eZDvo1LGTJEBJpQqWTBhfnK--xO1qEN1qH3mesj3MGmr20qFUYm8MUmfSkUFOUuXQBRQyMC7hKgf6hm41FNu22lU1i14&__tn__=GH-R&c[0]=AT0fsCuiqRSMwxcL6MvMXSCikj-52vwbTnqcuhoot2baX-ZO5TX-CfI-sv76yTr9OL_lPt-Z3bujrdQoQTooDCoG4H5dxMhPItb5hO5S_2pewAAezqI2vW53TtZ0QbQjj1leoRRBXXkdtqKNC4ai) (2021) - recording, mixing
- Urbex Archive by Helios Lyons (2019-2020) - site scouting and historical research [on request, example below]  
  
-->
![Urbex_School_1](urbex_school_1.png)
