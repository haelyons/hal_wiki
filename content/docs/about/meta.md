---
weight: 2
bookFlatSection: true
title: "Meta"
bookToc: false
---

> “What has been done, thought, written, or spoken is not culture; culture is only that fraction which is remembered.”  
> _Gary Taylor (The Clock of the Long Now)_

## Motivation
This is the third version of helioslyons.com. I started by making a [Squarespace](https://www.squarespace.com/) blog, which I later migrated to a [static blog generator](https://github.com/parksb/handmade-blog), followed by this [Hugo Book-theme](https://github.com/alex-shpak/hugo-book) static wiki. Based on that history this new website is intended to consist of a blog combined with a wiki providing documentation for my tools, and articles on topics of interest.

I feel that my current job as developer would be almost impossible without the huge array of blogs that I consult regularly for specific topics. I hope that this website can be a contribution to this digital garden. Below is a list of some of the sites that I cherish and consult frequently; I hope these in tandem with this website can serve as inspiration for your own personal wikis. If you know of other blogs or websites that you think are interesting, please let me know! 

## Some inspirations
- https://gzalo.com/en/ – detailed and up-to-date personal projects from Gonzalo Alterach (best use of the GitHub projects functionality that I have ever seen)
- https://grantshandy.github.io/ – Grant’s blog about programming and visual tools
- https://lethain.com/ – Will Larson’s blog about his writing, mainly on engineering in large orgs.
- https://lucidar.me/en/ – Great source for learning C++ and electronics from Lulu
- https://www.thirtythreeforty.net/ – breakdowns of building Linux distributions in Buildroot from George Hilliard

## License
This site is licensed under the Creative Commons [public domain (CC-0)](https://creativecommons.org/share-your-work/public-domain/cc0/) licence. This follows the philosophy of [copying and reproduction](https://en.wikipedia.org/wiki/LOCKSS) in which this website was created, potentially contributing to free and open source software, while costing me nothing.