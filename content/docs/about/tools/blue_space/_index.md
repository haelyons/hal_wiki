# Blue Space

*Blue Space* was built in MaxMSP taking inspiration from Nobuyasu Sakonda's [granular synthesis patch](http://formantbros.jp/sako/download.html), reworked for real-time performance and sound transformation. You can access the most recent version on [GitHub](https://github.com/haelyons/MaxMSP-Experiments), though you will need [Max](https://cycling74.com/products/max) to run it; a free client is available, as a subscription is only required for editing and saving the patch. Have fun!

You can read a more in depth explanation of the rationale, design, and functionality in the related _Building a real-time granular sound engine_ blog post.

![Blue_Space_Interface](blue_space_interface.png)