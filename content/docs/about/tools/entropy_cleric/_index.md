# Entropy Cleric

I built the _Entropy Cleric_ synthesizer from the ground up as an extension of my music technology coursework. The initial design only had standard synthesizer features (oscillators, filters, portamento, envelope, some weird distortion, etc). 

I expanded upon this with [stochastic music system](https://en.wikipedia.org/wiki/Iannis_Xenakis) which takes note toggle input from the ‘Spicey™ Note Generation’ tab in the top left, randomly selecting the next note to be played from those toggled along with adjustable interval range. The point was to have the synthesizer select the notes, which limited my compositional control to the timbral aspects of the sound (the mechanisms by which it is generated) to create a machine-collaboration scenario.

You can read a more in depth explanation of the rationale, design, and functionality in the related _Pareidolia: An algorithmic synthesizer in Max_ blog post. It is available on [GitHub](https://github.com/haelyons/MaxMSP-Experiments) and can be opened with [Max](https://cycling74.com/products/max), which is free for use (though you won't be able to save or edit).

![Entropy_Cleric](https://raw.githubusercontent.com/haelyons/Website-Content/master/ENTROPY%20CLERIC%202.png)