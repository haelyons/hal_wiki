+++
title = "Building this website"
subtitle = "A guide to setting up a static website with Hugo and GitLab Pages"
description = ""
tags = [
    "website",
    "continuous-integration",
]
date = "2023-07-08"
categories = [
    "Software",
]
menu = "main"
+++

This post is a short guide to setting up your own static website based on the Hugo framework using GitLab's free hosting to serve it either on yourwebsite.gitlab.io or a custom domain you own (helioslyons.com) in my case.

Not being a web developer, I took advantage of Hugo's great documentation, as well as GitLab's generous tier of free build servers and pre-built Docker images with Hugo installed to speed up development.

Roughly the steps consist of  
1. Installing Hugo, your chosen theme, customisation, and initialising the repo with `git init` 
2. Uploading this repository to GitLab (by setting the remote to the URL of a blank repo you've created there)
3. Setting up CI/CD to generate the site using a Docker image with Hugo
4. Adding a custom URL if you so choose

## Building Locally
Initialise the repository with Git – I uploaded it to GitHub and then linked the relevant repository to GitLab following their excellent documentation on this. This is unnecessary and only for my own organisation, the project could easily be initialised directly on GitLab if you wish! 

Before uploading to GitLab I spent a couple days thinking about what I wanted the site to look like, settling on the Hugo Book theme (typically used for documentation on source code) for its simplicity and neat presentation on mobile, and porting over my content. This mainly consists of editing the config.toml and markdown files – you can see changes live by hosting locally using command below (you can omit the --theme hugo-book command by setting theme: hugo-book in the site configuration.

```
hugo server --minify --theme hugo-book
```

## Host on GitLab
To host on GitLab, we're going to use GitLab's CI ([continuous integration](https://about.gitlab.com/topics/ci-cd/)) file .gitlab-ci.yml to link our site repository with a Docker image that has Hugo installed. With this we can generate the site, and tag the generated folder as an artefact. GitLab will serve this folder – please see the [Hugo image docs](https://gitlab.com/pages/hugo/).

Before setting up the pipeline, you need to make sure that the baseURL variable in your Hugo repository is set to your website URL, not example.org. Linking your existing GitLab repository is as easy as selecting the "Set up CI/CD" option, which will lead you to the Pipeline editor, where you can use the below .gitlab-ci.yml configuration:

Continuous integration is an umbrella term for "stuff that runs whenever your code changes." In this context our.gitlab-ci.yml file describes what should happen when we make changes to our repo – it will build our website in a Hugo Docker image from the master branch using the artefacts provided in the public/ repository. It will also inform you if your website was built correctly; you can validate the whole thing from your browser.

```
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

## That's it!
Your site should be up momentarily, though you might have to wait a bit for the changes to propagate.